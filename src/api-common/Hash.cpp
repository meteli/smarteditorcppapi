
#include <picosha2.h>
#include "Hash.hpp"

namespace se::api::common {

std::string Hash::make(const std::string& string) {
  const std::string hex = picosha2::hash256_hex_string(string);

  return hex.substr(0, 32);
}

}