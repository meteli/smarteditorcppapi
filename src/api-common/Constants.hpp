#pragma once

#include "Types.hpp"

namespace se::api::common {

struct Constants {
  inline static const std::string EMPTY_STRING = "";
  inline static const std::string ERROR_STRING = "#^@&$*@#&$^@#*&$^@*#&$@#^*$&@#";
  inline static const std::string NOT_IMPLEMENTED_STRING = "Not implemented";
};

}
