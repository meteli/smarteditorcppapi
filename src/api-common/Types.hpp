#pragma once

#include <string>
#include <variant>
#include <cstdint>

#include "Format.hpp"

namespace se::api::common {

using Byte = std::int8_t;
using UByte = std::uint8_t;
using Short = std::int16_t;
using UShort = std::uint16_t;
using Int = std::int32_t;
using UInt = std::uint32_t;
using Long = std::int64_t;
using ULong = std::uint64_t;
using Timestamp = Long;
using Id = Long;

struct CursorPos {
  std::size_t row;
  std::size_t column;

  [[nodiscard]] std::string toString() const {
    return fmt::format("CursorPos{{row = {}, column = {}}}", row, column);
  }

  [[nodiscard]] std::string toShortString() const {
    return fmt::format("{{{}, {}}}", row, column);
  }

  bool operator==(const CursorPos& other) {
    return row == other.row && column == other.column;
  }

  bool operator!=(const CursorPos& other) {
    return !(*this == other);
  }

  template <typename OStream>
  friend inline OStream& operator<<(OStream& os, const CursorPos& cursorPos) {
    os << cursorPos.toString();

    return os;
  }
};

}
