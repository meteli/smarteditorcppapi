#pragma once

#include <atomic>

#include "Types.hpp"

namespace se::api::common {

/**
 * @brief Simple thread safe id generator
 * @tparam T entity type
 */
template <typename T>
struct IdGenerator {
  using EntityType = T;

  static std::atomic<Id> id;

  /**
   * @return new id for object of the entity type @p T
   */
  static Id get() {
    return id++;
  }
};

template <typename T> std::atomic<Id> IdGenerator<T>::id{0};

}
