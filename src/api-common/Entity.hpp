#pragma once

#include <memory>

#include "Types.hpp"
#include "AdditionalTraits.hpp"

namespace se::api::common {

/**
 * @brief Base entity class
 *
 * @since 1.0.0
 */
struct Entity {
  virtual ~Entity() = default;
};


/**
 * @brief Entity that could be stored in a std::shared_ptr
 */
struct ShareableEntity : Entity, std::enable_shared_from_this<ShareableEntity> {
  /**
   * @brief Helper method for std::shared_ptr's downcasting
   * @tparam Down child class to which \c this should be downcasted
   * @return \c this that is downcasted to std::shared_ptr<\c Down> or \c nullptr
   */
  template <typename Down>
  std::shared_ptr<Down> getSharedFromThisAs() {
    return std::dynamic_pointer_cast<Down>(shared_from_this());
  }

  template <typename Down>
  std::shared_ptr<Down> getSharedFromThisAs() const {
    return std::dynamic_pointer_cast<Down>(shared_from_this());
  }
};

using ShareableEntityPtr = std::shared_ptr<ShareableEntity>;

}
