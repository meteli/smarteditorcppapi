#pragma once

#include "Types.hpp"

namespace se::api::common {

struct Hash {
  static std::string make(const std::string& string);
};

}
