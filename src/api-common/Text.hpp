#pragma once

#include <vector>
#include <functional>

#include "Types.hpp"
#include "Format.hpp"

#include <boost/functional/hash.hpp>

namespace se::api::common {

struct Text {
  std::vector<std::string> data;

  template <typename OStream>
  friend inline OStream& operator<<(OStream& os, const Text& text) {
    os << fmt::format("Text[{}]", text.data.size());

    return os;
  }
};

}

namespace std {

template <>
struct hash<se::api::common::Text> {
  std::size_t operator()(const se::api::common::Text& text) const {
    return boost::hash_range(text.data.begin(), text.data.end());
  }
};

}
