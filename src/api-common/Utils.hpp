#pragma once

#include <sstream>
#include <iomanip>
#include <ctime>
#include <locale>
#include <iostream>
#include <chrono>
#include <memory>
#include <utility>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>
#include <array>
#include <forward_list>
#include <initializer_list>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/join.hpp>

#include "AdditionalTraits.hpp"
#include "Types.hpp"
#include "Text.hpp"
#include "Format.hpp"

namespace se::api::common {

/**
 * Try to make static_cast of a value from V to T (sometimes mutes compiler warnings)
 *
 * @tparam T result type
 * @tparam V value type
 * @param value
 * @return Casted value
 */
template <typename T, typename V>
constexpr auto cast(V value) {
  return static_cast<T>(value);
}

/**
 * Formats a timestamp
 *
 * @param timestamp
 * @param format Format string (default: %Y-%m-%d %H:%M:%S)
 * @return Formatted string
 */
inline std::string formatTime(Timestamp timestamp, const std::string& format = "%Y-%m-%d %H:%M:%S") {
  std::stringstream ss;
  auto t = cast<std::time_t>(timestamp);

  ss << std::put_time(std::gmtime(&t), format.c_str());

  return ss.str();
}

template <typename R, typename H>
inline std::size_t hashRange(R&& range, H hashProvider) {
  std::size_t seed = 0;

  for (auto&& v : range) {
    boost::hash_combine(seed, hashProvider(v));
  }

  return seed;
}

inline std::string formatTimestampInMillis(Timestamp timestamp) {
  Long shortTimestamp = timestamp / 1000;
  Long ms = timestamp % 1000;

  std::stringstream ss;
  auto t = cast<std::time_t>(shortTimestamp);

  ss << std::put_time(std::gmtime(&t), "%Y-%m-%d %H:%M:%S");

  return fmt::format("{}.{:0>3}", ss.str(), ms);
}

inline Timestamp getCurrentTimestamp() {
  return cast<Timestamp>(
      std::chrono::duration_cast<std::chrono::seconds>(
          std::chrono::system_clock::now().time_since_epoch()).count());
}

inline Timestamp getCurrentTimestampInMillis() {
  return cast<Timestamp>(
      std::chrono::duration_cast<std::chrono::milliseconds>(
          std::chrono::system_clock::now().time_since_epoch()).count());
}

#define finally(id, ...)std::shared_ptr<void(*)(void*)> finally##id(nullptr, [&](void*)__VA_ARGS__)

}

template <typename... Ts>
void unused(Ts&& ...) {
}

template <typename Container, typename = se::api::common::enableIf<
    se::api::common::isInstanceOf<Container, std::vector>
    || se::api::common::isInstanceOf<Container, std::list>
    || se::api::common::isInstanceOf<Container, std::set>
    || se::api::common::isInstanceOf<Container, std::unordered_set>
    || se::api::common::isInstanceOf<Container, std::forward_list>
    || se::api::common::isInstanceOf<Container, std::initializer_list>
>>
inline std::string containerToString(const Container& items, std::size_t maxElements = 0) {
  static const std::size_t DEFAULT_MAX_ELEMENTS = 10;

  if (maxElements == 0) {
    maxElements = DEFAULT_MAX_ELEMENTS;
  }

  auto end = std::end(items);
  auto it = std::begin(items);
  auto length = se::api::common::cast<std::size_t>(std::distance(it, end));

  if (it == end) {
    return "[]";
  }

  std::stringstream ss;

  if (length > maxElements) {
    ss << "(" << maxElements << "/" << length << ")";
  } else {
    ss << "(" << length << ")";
  }

  ss << "[" << (*it);

  std::size_t index = 1;
  ++it;

  for (; it != end && index < maxElements; ++it) {
    ss << ", " << (*it);

    ++index;
  }

  ss << "]";

  return ss.str();
}

template <typename Container, typename ToShortStringConverter, typename = se::api::common::enableIf<
    se::api::common::isInstanceOf<Container, std::vector>
    || se::api::common::isInstanceOf<Container, std::list>
    || se::api::common::isInstanceOf<Container, std::set>
    || se::api::common::isInstanceOf<Container, std::forward_list>
>>
inline std::string containerToShortString(const Container& items, ToShortStringConverter converter) {
  auto end = std::end(items);
  auto it = std::begin(items);

  if (it == end) {
    return "[]";
  }

  std::stringstream ss;

  ss << "[";

  for (; it != end; ++it) {
    ss << converter(*it);
  }

  ss << "]";

  return ss.str();
}

template <typename Container, typename = se::api::common::enableIf<
    se::api::common::isInstanceOf<Container, std::map>
    || se::api::common::isInstanceOf<Container, std::unordered_map>
>>
inline std::string associativeContainerToString(const Container& items) {
  static const std::size_t MAX_ELEMENTS = 10;
  auto end = std::end(items);
  auto it = std::begin(items);
  auto length = se::api::common::cast<std::size_t>(std::distance(it, end));

  if (it == end) {
    return "[]";
  }

  std::stringstream ss;

  if (length > MAX_ELEMENTS) {
    ss << "(" << MAX_ELEMENTS << "/" << length << ")";
  } else {
    ss << "(" << length << ")";
  }

  ss << "[{" << it->first << ", " << it.second << "}";

  std::size_t index = 1;
  ++it;

  for (; it != end && index < MAX_ELEMENTS; ++it) {
    ss << ", {" << it->first << ", " << it.second << "}";

    ++index;
  }

  ss << "]";

  return ss.str();
}

template <typename Container, typename KeyToShortStringConverter, typename ValueToShortStringConverter, typename = se::api::common::enableIf<
    se::api::common::isInstanceOf<Container, std::map>
    || se::api::common::isInstanceOf<Container, std::unordered_map>
>>
inline std::string
associativeContainerToShortString(const Container& items, KeyToShortStringConverter kConverter,
                                  ValueToShortStringConverter vConverter) {
  auto end = std::end(items);
  auto it = std::begin(items);

  if (it == end) {
    return "[]";
  }

  std::stringstream ss;

  ss << "[";

  for (; it != end; ++it) {
    ss << "{" << kConverter(it->first) << " -> " << vConverter(it->second) << "}";
  }

  ss << "]";

  return ss.str();
}

template <typename OStream, typename O>
inline OStream& operator<<(OStream& os, const std::vector<O>& items) {
  os << containerToString(items);

  return os;
}

template <typename OStream, typename O>
inline OStream& operator<<(OStream& os, const std::list<O>& items) {
  os << containerToString(items);

  return os;
}

template <typename OStream, typename O>
inline OStream& operator<<(OStream& os, const std::set<O>& items) {
  os << containerToString(items);

  return os;
}

template <typename OStream, typename O>
inline OStream& operator<<(OStream& os, const std::forward_list<O>& items) {
  os << containerToString(items);

  return os;
}

template <typename OStream, typename K, typename V>
inline OStream& operator<<(OStream& os, const std::map<K, V>& items) {
  os << associativeContainerToString(items);

  return os;
}

template <typename OStream, typename K, typename V>
inline OStream& operator<<(OStream& os, const std::unordered_map<K, V>& items) {
  os << associativeContainerToString(items);

  return os;
}

#include <boost/hana.hpp>

namespace se::api::common {
constexpr auto optionalToString = [](auto&& o) {
  namespace hana = boost::hana;
  using namespace std::string_literals;

  auto ConvertibleByToStringFunction = hana::is_valid([](auto&& o) -> decltype(toString(o)) {
  });
  auto ConvertibleByToStringMethod = hana::is_valid([](auto&& o) -> decltype(o.toString()) {
  });
  auto ConvertibleByPointerToStringMethod = hana::is_valid([](auto&& o) -> decltype(o->toString()) {
  });
  auto ConvertibleByStdToString = hana::is_valid([](auto&& o) -> decltype(std::to_string(o)) {
  });
  auto ConvertibleByContainerToString = hana::is_valid([](auto&& o) -> decltype(containerToString(o)) {
  });
  auto ConvertibleByAssociativeContainerToString = hana::is_valid(
      [](auto&& o) -> decltype(associativeContainerToString(o)) {
      });
  auto ConvertibleByFmt = hana::is_valid([](auto&& o) -> decltype(fmt::format(o)) {
  });

  using type = std::decay_t<decltype(o)>;

  if constexpr (decltype(ConvertibleByToStringFunction(o))::value) {
    return toString(o);
  } else if constexpr (decltype(ConvertibleByToStringMethod(o))::value) {
    return o.toString();
  } else if constexpr (decltype(ConvertibleByPointerToStringMethod(o))::value) {
    return "*"s + o->toString();
  } else if constexpr (decltype(ConvertibleByStdToString(o))::value) {
    return std::to_string(o);
  } else if constexpr (decltype(ConvertibleByContainerToString(o))::value) {
    return containerToString(o);
  } else if constexpr (decltype(ConvertibleByAssociativeContainerToString(o))::value) {
    return associativeContainerToString(o);
  } else if constexpr (std::is_null_pointer_v<type>) {
    return "nullptr"s;
  } else if constexpr (std::is_void_v<type>) {
    return "void"s;
  } else if constexpr (decltype(ConvertibleByFmt(o))::value) {
    return fmt::format(o);
  } else {
    return "[DATA]"s;
  }
};
}