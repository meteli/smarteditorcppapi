#pragma once

#include <type_traits>

namespace se::api::common {

template <typename... Ts>
struct TypeList {
  static constexpr auto size = sizeof...(Ts);
};

using EmptyList = TypeList<>;

struct None {
};

using True = std::true_type;

using False = std::false_type;

template <typename T, typename U>
struct Pair {
  using first = T;
  using second = U;
};

using FailPair = Pair<None, None>;

template <typename Base, typename T>
constexpr bool isBaseOf = std::is_base_of<Base, T>::value;

template <typename T>
constexpr bool isAbstract = std::is_abstract<T>::value;

template <typename T>
constexpr bool isPolymorphic = std::is_polymorphic<T>::value;

template <typename T, typename U>
constexpr bool isSame = std::is_same<T, U>::value;

template <template <class...> class,
    template <class...> class>
struct Same : False {

};

template <template <class...> class C>
struct Same<C, C> : True {

};

template <typename From, typename To>
constexpr bool isConvertible = std::is_convertible<From, To>::value;

template <bool Cond>
using enableIf = std::enable_if_t<Cond>;

template <typename T>
struct GetInstancingTypes {
  using apply = EmptyList;
};

template <template <class...> class C, typename... Ts>
struct GetInstancingTypes<C<Ts...>> {
  using apply = TypeList<Ts...>;
};

template <typename T>
constexpr bool hasInstancingTypes = !isSame<GetInstancingTypes<T>, EmptyList>;

template <template <class...> class C, typename... Ts>
constexpr auto InstanceWithTypesFromHelper(TypeList<Ts...> = TypeList<Ts...>()) -> C<Ts...> {
  return {};
}

template <template <class...> class C, typename... Ts>
constexpr auto InstanceWithTypesFromHelper(...) -> None {
  return {};
}

template <template <class...> class C, typename T>
using InstanceWithTypesFrom = decltype(InstanceWithTypesFromHelper<C>(typename GetInstancingTypes<T>::apply()));

template <template <class...> class C>
struct Instance {
  template <typename T>
  using WithTypesFrom = InstanceWithTypesFrom<C, T>;
};

template <template <class...> class C, typename T>
struct IsInstanceHelper : False {

};

template <template <class...> class C1,
    template <class...> class C2, typename... Ts>
struct IsInstanceHelper<C1, C2<Ts...>> : Same<C1, C2> {

};

template <typename T, template <class...> class C>
constexpr bool isInstanceOf = IsInstanceHelper<C, T>::value;

template <typename T, typename... Ts>
constexpr auto HeadTailHelper(TypeList<T, Ts...> = TypeList<T, Ts...>()) {
  return Pair<T, TypeList<Ts...>>();
}

template <typename TL, typename Enable = void>
struct HeadTailHelperStruct : FailPair {
};

template <typename TL>
struct HeadTailHelperStruct<TL, enableIf<isInstanceOf<TL, TypeList> && (TL::size > 0)>> : decltype(HeadTailHelper(
    TL())) {
};

template <typename TL>
using Head = typename HeadTailHelperStruct<TL>::first;

template <typename TL>
using Tail = typename HeadTailHelperStruct<TL>::second;

template <typename T, typename U>
using enableIfIsBaseOf = enableIf<isBaseOf<T, U>>;

template <typename T>
using FirstUnderlying = Head<typename GetInstancingTypes<T>::apply>;

template <typename T>
using GetFirstInstancingType = Head<typename GetInstancingTypes<T>::apply>;

template <typename T, template <class> class Optional>
using OptionalUnderlying = typename
std::conditional_t<
    isInstanceOf<T, Optional>,
    Head<typename GetInstancingTypes<T>::apply>,
    T
>;

}
