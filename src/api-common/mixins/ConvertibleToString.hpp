#pragma once

#include <iostream>
#include <type_traits>
#include <memory>
#include <string>

#include <api-common/Types.hpp>
#include <api-common/AdditionalTraits.hpp>

namespace se::api::common {

struct ConvertibleToString {
  virtual const std::string toString() const = 0;

  virtual ~ConvertibleToString() = default;
};

struct ConvertibleToShortString {
  virtual const std::string toShortString() const = 0;

  virtual ~ConvertibleToShortString() = default;
};

template <typename T>
using enableIfIsConvertibleToString = common::enableIfIsBaseOf<ConvertibleToString, T>;

}

template <typename O, typename = se::api::common::enableIfIsConvertibleToString<O>>
inline std::string toString(const O& o) {
  return o.toString();
}

template <typename O, typename = se::api::common::enableIfIsConvertibleToString<O>>
inline std::string toString(const O* o) {
  return std::string("*") + ((o) ? o->toString() : "nullptr");
}

template <typename O, typename = se::api::common::enableIfIsConvertibleToString<O>>
inline std::string toString(std::shared_ptr<O> o) {
  return std::string("<*>") + ((o) ? o->toString() : "nullptr");
}

template <typename OStream, typename O, typename = se::api::common::enableIfIsConvertibleToString<O>>
inline OStream& operator<<(OStream& os, const O& o) {
  os << toString(o);

  return os;
}

template <typename OStream, typename O, typename = se::api::common::enableIfIsConvertibleToString<O>>
inline OStream& operator<<(OStream& os, const O* o) {
  os << toString(o);

  return os;
}

template <typename OStream, typename O, typename = se::api::common::enableIfIsConvertibleToString<O>>
inline OStream& operator<<(OStream& os, std::shared_ptr<O> o) {
  os << toString(o);

  return os;
}