#pragma once

#include <api-common/Types.hpp>

namespace se::api::common {

/**
 * An abstract class that describes all classes with a name
 *
 * @since 1.0.0
 */
struct HasName {
  virtual const std::string& getName() const& = 0;
};

/**
 * A mixin class that implements @ref HasName
 */
class HasNameMixin : HasName {
protected:
  std::string name_;

public:

  explicit HasNameMixin(const std::string& name);

  const std::string& getName() const& override;
};

}
