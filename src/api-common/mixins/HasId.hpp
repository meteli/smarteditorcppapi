#pragma once

#include <api-common/Types.hpp>

namespace se::api::common {

/**
 * An abstract class that describes all classes with an id
 *
 * @since 1.0.0
 */
struct HasId {
  virtual Id getId() const = 0;
};

/**
 * A mixin class that implements @ref HasId
 */
class HasIdMixin : HasId {
protected:
  Id id_;

public:

  explicit HasIdMixin(Id id);

  Id getId() const override;
};

}
