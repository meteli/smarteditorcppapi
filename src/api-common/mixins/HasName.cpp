#include "HasName.hpp"

namespace se::api::common {

HasNameMixin::HasNameMixin(const std::string& name) : name_{name} {
}

const std::string& HasNameMixin::getName() const& {
  return name_;
}

}