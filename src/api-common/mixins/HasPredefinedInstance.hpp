#pragma once

#include <memory>

namespace se::api::common {

template <typename T>
struct HasPredefinedInstance {
  template <typename... Args>
  static auto getInstance(Args&& ... args) {
    static auto instance = std::make_shared<T>(std::forward(args)...);

    return instance;
  }
};

}
