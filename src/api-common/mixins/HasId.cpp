#include "HasId.hpp"

namespace se::api::common {

HasIdMixin::HasIdMixin(common::Id id) : id_{id} {
}

common::Id HasIdMixin::getId() const {
  return id_;
}

}