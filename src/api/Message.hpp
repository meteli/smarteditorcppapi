#pragma once

#include <memory>
#include <string>

#include <api-common/Entity.hpp>
#include <api-common/mixins/ConvertibleToString.hpp>

namespace se::api {

namespace c = common;

/**
 * A base message class
 */
struct Message : c::Entity, c::ConvertibleToString {
  static const std::string getApiVersion();
};

//  std::enable_shared_from_this has non virtual destructor, so disable "-Wnon-virtual-dtor"
#if defined(__clang__)
#elif defined(__GNUC__) || defined(__GNUG__)
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wnon-virtual-dtor"
#elif defined(_MSC_VER)
#endif

struct ShareableMessage : Message, std::enable_shared_from_this<ShareableMessage> {
  template <typename Down>
  std::shared_ptr<Down> getSharedFromThisAs() {
    return std::dynamic_pointer_cast<Down>(shared_from_this());
  }
};

#if defined(__clang__)
#elif defined(__GNUC__) || defined(__GNUG__)
#   pragma GCC diagnostic pop
#elif defined(_MSC_VER)
#endif

using ShareableMessagePtr = std::shared_ptr<ShareableMessage>;

}
