#pragma once

#include <unordered_map>

#include <api-common/Types.hpp>

namespace se::api {

namespace c = common;

struct StatusesRecord {
  struct Status {
    enum Type { NONE, RUNNING, FINISHED };

    Type type;
    c::Timestamp timestamp; // in ms
  };

  enum JobType { PATTERNS_PROCESSING, TEXT_TRANSFORMATIONS_GENERATING };

  static const std::string& jobTypeToString(const JobType jobType) {
    static const std::unordered_map<JobType, std::string> converter{
        {JobType::PATTERNS_PROCESSING,             "patternsProcessing"},
        {JobType::TEXT_TRANSFORMATIONS_GENERATING, "textTransformationsGenerating"}
    };

    return converter.find(jobType)->second;
  }

  static const std::string& statusTypeToString(const Status::Type statusType) {
    static const std::unordered_map<Status::Type, std::string> converter{
        {Status::Type::NONE,     "None"},
        {Status::Type::RUNNING,  "Running"},
        {Status::Type::FINISHED, "Finished"}
    };

    return converter.find(statusType)->second;
  }

  c::Id documentId;
  std::string documentPath;

  std::unordered_map<JobType, Status> items;
};

}
