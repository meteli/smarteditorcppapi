#pragma once

#include <string>

#include <api/Message.hpp>

namespace se::api {

struct GetSession final : ShareableMessage {
  inline static const std::string type = "GetSession";

  std::string application;
  std::string applicationKey;

  GetSession(std::string application, std::string applicationKey);

  const std::string toString() const override;
};

}
