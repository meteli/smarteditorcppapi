#include "GetSession.hpp"

#include <api-common/Format.hpp>

namespace se::api {

GetSession::GetSession(std::string application, std::string applicationKey)
    : application{std::move(application)}, applicationKey{std::move(applicationKey)} {
}

const std::string GetSession::toString() const {
  return fmt::format("{}{{api = '{}', app = '{}', appKey = '{}'}}", type, getApiVersion(), application,
                     applicationKey);
}

}