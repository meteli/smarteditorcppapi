#pragma once

#include <api/Message.hpp>

namespace se::api {

struct GetTextTransformations final : ShareableMessage {
  inline static const std::string type = "GetTextTransformations";

  std::string sessionId;
  std::string document;

  GetTextTransformations(std::string sessionId, std::string document);

  const std::string toString() const override;
};

}
