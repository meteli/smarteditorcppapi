#include <utility>

#include <utility>

#include "MoveCursor.hpp"

#include <api-common/Format.hpp>

namespace se::api {

const std::string MoveCursor::toString() const {
  return fmt::format("{}{{api = '{}', sessionId = '{}', document = '{}', cursorPos = {}}}",
                     type, getApiVersion(), sessionId, document, cursorPos);
}

MoveCursor::MoveCursor(std::string sessionId, std::string document, c::CursorPos cursorPos)
    : sessionId{std::move(sessionId)}, document{std::move(document)}, cursorPos{cursorPos} {
}

}