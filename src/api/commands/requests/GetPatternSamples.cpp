#include "GetPatternSamples.hpp"

#include <api-common/Format.hpp>

namespace se::api {

const std::string GetPatternSamples::toString() const {
  return fmt::format("{}{{api = '{}', sessionId = '{}', document = '{}'}}",
                     type, getApiVersion(), sessionId, document);
}

GetPatternSamples::GetPatternSamples(std::string sessionId, std::string document)
    : sessionId{std::move(sessionId)}, document{std::move(document)} {
}

}