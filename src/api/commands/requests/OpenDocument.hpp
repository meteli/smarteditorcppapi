#pragma once

#include <vector>

#include <api/Message.hpp>

namespace se::api {

namespace c = common;

struct OpenDocument final : ShareableMessage {
  inline static const std::string type = "OpenDocument";

  std::string sessionId;
  std::string path;
  std::vector<std::string> visibleText;
  std::size_t visibleTextStartLine;
  c::CursorPos cursorPos;

  OpenDocument(std::string sessionId, std::string path, std::vector<std::string> visibleText,
               std::size_t visibleTextStartLine, c::CursorPos cursorPos);

  const std::string toString() const override;
};

}