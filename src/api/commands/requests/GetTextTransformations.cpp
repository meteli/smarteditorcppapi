#include "GetTextTransformations.hpp"

#include <api-common/Format.hpp>

namespace se::api {

const std::string GetTextTransformations::toString() const {
  return fmt::format("{}{{api = '{}', sessionId = '{}', document = '{}'}}",
                     type, getApiVersion(), sessionId, document);
}

GetTextTransformations::GetTextTransformations(std::string sessionId, std::string document)
    : sessionId{std::move(sessionId)}, document{std::move(document)} {
}

}