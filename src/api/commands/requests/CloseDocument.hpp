#pragma once

#include <api/Message.hpp>

namespace se::api {

/**
 * A command that describes a document close event
 */
struct CloseDocument final : ShareableMessage {
  inline static const std::string type = "CloseDocument";

  std::string sessionId;
  std::string path;

  /**
   * Constructs the new `CloseDocument` command
   *
   * @param sessionId Current session's id
   * @param path      Document's path to close
   */
  CloseDocument(std::string sessionId, std::string path);

  const std::string toString() const override;
};

}