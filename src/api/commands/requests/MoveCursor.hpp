#pragma once

#include <api/Message.hpp>
#include <api-common/Types.hpp>

namespace se::api {

namespace c = common;

struct MoveCursor final : ShareableMessage {
  inline static const std::string type = "MoveCursor";

  std::string sessionId;
  std::string document;
  c::CursorPos cursorPos;

public:

  MoveCursor(std::string sessionId, std::string document, c::CursorPos cursorPos);

  const std::string toString() const override;

};

}
