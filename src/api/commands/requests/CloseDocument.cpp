#include "CloseDocument.hpp"

#include <api-common/Format.hpp>

namespace se::api {

CloseDocument::CloseDocument(std::string sessionId, std::string path) :
    sessionId{std::move(sessionId)}, path{std::move(path)} {
}

const std::string CloseDocument::toString() const {
  return fmt::format("{}{{api = '{}', sid = '{}', path = '{}'}}", type, getApiVersion(), sessionId, path);
}

}