#include <utility>

#include "ChangeText.hpp"

#include <api-common/Format.hpp>

namespace se::api {

const std::string ChangeText::toString() const {
  return fmt::format(
      "{}{{api = '{}', sid = '{}', path = '{}', visibleText{{size = {}, startLine = {}, cursorPos = {}}}}}",
      type, getApiVersion(), sessionId, path, visibleText.size(), visibleTextStartLine,
      cursorPos);
}

ChangeText::ChangeText(std::string sessionId, std::string path,
                       std::vector<std::string> visibleText,
                       std::size_t visibleTextStartLine, c::CursorPos cursorPos)
    : sessionId{std::move(sessionId)}, path{std::move(path)}, visibleText{std::move(visibleText)},
      visibleTextStartLine{visibleTextStartLine},
      cursorPos{cursorPos} {
}

}