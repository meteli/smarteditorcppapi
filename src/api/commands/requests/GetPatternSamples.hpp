#pragma once

#include <api/Message.hpp>

namespace se::api {

struct GetPatternSamples final : ShareableMessage {
  inline static const std::string type = "GetPatternSamples";

  std::string sessionId;
  std::string document;

  GetPatternSamples(std::string sessionId, std::string document);

  const std::string toString() const override;
};

}
