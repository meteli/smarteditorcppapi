#pragma once

#include <vector>

#include <api/Message.hpp>

namespace se::api {

namespace c = common;

/**
 * A command that describes a text change event
 */
struct ChangeText final : ShareableMessage {
  inline static const std::string type = "ChangeText";

  std::string sessionId;
  std::string path;
  std::vector<std::string> visibleText;
  std::size_t visibleTextStartLine;
  c::CursorPos cursorPos;

  /**
   * Constructs the new `ChangeText` command
   *
   * @param sessionId            current session id
   * @param path                 document's path
   * @param visibleText          document's visible text
   * @param visibleTextStartLine document's visible text start line index
   * @param cursorPos            current cursor position
   */
  ChangeText(std::string sessionId, std::string path, std::vector<std::string> visibleText,
             std::size_t visibleTextStartLine, c::CursorPos cursorPos);

  const std::string toString() const override;

};

}