#pragma once

#include <api/Message.hpp>
#include <api-common/Types.hpp>

namespace se::api {

namespace c = common;

struct Ping final : ShareableMessage {
  inline static const std::string type = "Ping";

  std::string sessionId;
  c::Timestamp timestamp;

  Ping(std::string sessionId, c::Timestamp timestamp);

  const std::string toString() const override;
};

}