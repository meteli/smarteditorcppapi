#include "Ping.hpp"
#include <api-common/Utils.hpp>

namespace se::api {

Ping::Ping(std::string sessionId, c::Timestamp timestamp) : sessionId{std::move(sessionId)}, timestamp{timestamp} {
}

const std::string Ping::toString() const {
  return fmt::format("{}{{api = '{}', sid = '{}', timestamp = '{}'}}",
                     type, getApiVersion(), sessionId, c::formatTimestampInMillis(timestamp));
}

}