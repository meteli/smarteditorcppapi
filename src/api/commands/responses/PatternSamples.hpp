#pragma once

#include <vector>

#include <api/Message.hpp>

namespace se::api {

struct PatternSamples final : ShareableMessage {
  struct Item {
    std::size_t lineIndex;
  };

  struct MultiLineItem {
    std::size_t fromLine;
    std::size_t toLine;
  };

  struct Anchor {
    std::size_t from;
    std::size_t to;
  };

  struct AnchorGroup {
    std::size_t lineIndex;
    std::vector<Anchor> items;
  };

  struct Separator {
    std::size_t from;
    std::size_t to;
  };

  struct SeparatorGroup {
    std::size_t lineIndex;
    std::vector<Separator> items;
  };

  inline static const std::string type = "PatternSamples";

  std::vector<Item> items;
  std::vector<MultiLineItem> multiLineItems;
  std::vector<AnchorGroup> anchors;
  std::vector<SeparatorGroup> separators;

  PatternSamples(std::vector<Item> items, std::vector<MultiLineItem> multiLineItems,
                 std::vector<AnchorGroup> anchors, std::vector<SeparatorGroup> separators);

  const std::string toString() const override;
};

}
