#include "MoveCursorResult.hpp"

#include <api-common/Format.hpp>

namespace se::api {

MoveCursorResult::MoveCursorResult(StatusesRecord statuses) : statuses{std::move(statuses)} {
}

const std::string MoveCursorResult::toString() const {
  return fmt::format("{}{{api = '{}'}}", type, getApiVersion());
}

}