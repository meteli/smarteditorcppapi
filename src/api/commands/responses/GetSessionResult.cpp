#include "GetSessionResult.hpp"

#include <api-common/Format.hpp>

namespace se::api {

GetSessionResult::GetSessionResult(std::string sessionId) : sessionId{std::move(sessionId)} {
}

const std::string GetSessionResult::toString() const {
  return fmt::format("{}{{api = '{}', sid = '{}'}}", type, getApiVersion(), sessionId);
}

}