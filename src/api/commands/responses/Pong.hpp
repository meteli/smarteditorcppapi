#pragma once

#include <vector>

#include <api/Message.hpp>
#include <api/StatusesRecord.hpp>

namespace se::api {

namespace c = common;

struct Pong final : public ShareableMessage {
  inline static const std::string type = "Pong";

  c::Timestamp timestamp;
  std::vector<StatusesRecord> statuses;

  Pong(c::Timestamp timestamp, std::vector<StatusesRecord> statusRecords);

  const std::string toString() const override;
};

}
