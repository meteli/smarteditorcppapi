#pragma once

#include <api/Message.hpp>
#include <api/StatusesRecord.hpp>

namespace se::api {

struct OpenDocumentResult final : ShareableMessage {
  inline static const std::string type = "OpenDocumentResult";

  StatusesRecord statuses;

  explicit OpenDocumentResult(StatusesRecord statuses);

  const std::string toString() const override;
};

}
