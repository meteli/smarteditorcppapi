#include "Ok.hpp"

#include <api-common/Format.hpp>

namespace se::api {

const std::string Ok::toString() const {
  return fmt::format("{}{{api = '{}'}}", type, getApiVersion());
}

}