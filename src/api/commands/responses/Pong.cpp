#include "Pong.hpp"
#include <api-common/Utils.hpp>

namespace se::api {

Pong::Pong(c::Timestamp timestamp, std::vector<StatusesRecord> statusRecords) :
    timestamp{timestamp}, statuses{std::move(statusRecords)} {
}

const std::string Pong::toString() const {
  return fmt::format("{}{{api = '{}', timestamp = '{}', statuses = {}}}", type, getApiVersion(),
                     c::formatTimestampInMillis(timestamp), statuses.size());
}

}