#pragma once

#include <api/Message.hpp>

namespace se::api {

struct GetSessionResult final : ShareableMessage {
  inline static const std::string type = "Session";

  std::string sessionId;

  explicit GetSessionResult(std::string sessionId);

  const std::string toString() const override;
};

}
