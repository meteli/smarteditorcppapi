#pragma once

#include <api-common/mixins/HasPredefinedInstance.hpp>
#include <api/Message.hpp>

namespace se::api {

namespace c = common;

/**
 * A class to store a message -- the "good" processing result
 */
struct Ok final : ShareableMessage, c::HasPredefinedInstance<Ok> {
  inline static const std::string type = "Ok";

  const std::string toString() const override;
};

}
