#pragma once

#include <memory>

#include <api/Message.hpp>

namespace se::api {

struct Error : ShareableMessage {
  inline static const std::string type = "Error";

  std::string errorMessage;

  explicit Error(std::string errorMessage);

  const std::string toString() const override;

  template <typename... Args>
  static auto create(const std::string& string, const Args& ... args) {
    return std::make_shared<Error>(fmt::format(string, args...));
  }
};

}