#include "OpenDocumentResult.hpp"

#include <api-common/Format.hpp>

namespace se::api {

OpenDocumentResult::OpenDocumentResult(StatusesRecord statuses) : statuses{std::move(statuses)} {
}

const std::string OpenDocumentResult::toString() const {
  return fmt::format("{}{{api = '{}'}}", type, getApiVersion());
}

}