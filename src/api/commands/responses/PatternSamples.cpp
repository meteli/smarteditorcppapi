#include "PatternSamples.hpp"

#include <api-common/Format.hpp>

namespace se::api {

const std::string PatternSamples::toString() const {
  return fmt::format("{}{{api = '{}', items: {}, multiLineItems: {}, anchors: {}, separators: {}}}",
                     type, getApiVersion(), items.size(), multiLineItems.size(), anchors.size(), separators.size());
}

PatternSamples::PatternSamples(std::vector<PatternSamples::Item> items,
                               std::vector<PatternSamples::MultiLineItem> multiLineItems,
                               std::vector<AnchorGroup> anchors,
                               std::vector<SeparatorGroup> separators) :
    items{std::move(items)}, multiLineItems{std::move(multiLineItems)}, anchors{std::move(anchors)},
    separators{std::move(separators)} {
}

}