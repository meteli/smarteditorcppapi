#include "ChangeTextResult.hpp"

#include <api-common/Format.hpp>

namespace se::api {

ChangeTextResult::ChangeTextResult(StatusesRecord statuses) : statuses{std::move(statuses)} {
}

const std::string ChangeTextResult::toString() const {
  return fmt::format("{}{{api = '{}'}}", type, getApiVersion());
}

}