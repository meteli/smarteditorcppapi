#pragma once

#include <vector>

#include <api/Message.hpp>

namespace se::api {

struct TextTransformations final : ShareableMessage {
  inline static const std::string type = "TextTransformations";

  using Diffs = std::vector<std::pair<std::string, std::string>>;

  /// [(line index, text)]
  std::vector<std::pair<std::size_t, std::string>> items;
  /// [(line index, diffs)]
  std::vector<std::pair<std::size_t, Diffs>> extended;

  explicit TextTransformations(std::vector<std::pair<std::size_t, std::string>> items,
                               std::vector<std::pair<std::size_t, Diffs>> extended);

  const std::string toString() const override;
};

}
