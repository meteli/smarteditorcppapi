#pragma once

#include <api/Message.hpp>
#include <api/StatusesRecord.hpp>

namespace se::api {

struct ChangeTextResult final : ShareableMessage {
  inline static const std::string type = "ChangeTextResult";

  StatusesRecord statuses;

  explicit ChangeTextResult(StatusesRecord statuses);

  const std::string toString() const override;
};

}