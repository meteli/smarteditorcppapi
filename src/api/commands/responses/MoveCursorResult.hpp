#pragma once

#include <api/Message.hpp>
#include <api/StatusesRecord.hpp>

namespace se::api {

struct MoveCursorResult final : ShareableMessage {
  inline static const std::string type = "MoveCursorResult";

  StatusesRecord statuses;

  explicit MoveCursorResult(StatusesRecord statuses);

  const std::string toString() const override;
};

}