#include "Error.hpp"

namespace se::api {

Error::Error(std::string errorMessage) : errorMessage{std::move(errorMessage)} {
}

const std::string Error::toString() const {
  return fmt::format("{}{{api = '{}', msg = '{}'}}", type, getApiVersion(), errorMessage);
}

}