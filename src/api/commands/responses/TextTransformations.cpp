#include "TextTransformations.hpp"

#include <api-common/Format.hpp>

namespace se::api {

const std::string TextTransformations::toString() const {
  return fmt::format("{}{{api = '{}', items/extended = {}}}", type, getApiVersion(), items.size());
}

TextTransformations::TextTransformations(std::vector<std::pair<std::size_t, std::string>> items,
                                         std::vector<std::pair<std::size_t, TextTransformations::Diffs>> extended)
    : items{std::move(items)}, extended{std::move(extended)} {
}

}