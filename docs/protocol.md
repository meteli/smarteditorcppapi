## JSON protocol

### List

* [`Ok`](#ok-message)
* [`Error`](#error-message)
* [`Ping`](#ping-message) => [`Pong`](#pong-message) | [`Error`](#error-message)
  * [`Statuses array`](#statuses-array)
  * [`Jobs map possible keys`](#jobs-map-possible-keys)
  * [`Jobs object (value)`](#jobs-object-value)
* [`GetSession`](#getsession-message) => [`Session`](#session-getsession-messages-result) | [`Error`](#error-message)
* [`OpenDocument`](#opendocument-message) => [`OpenDocumentResult`](#opendocumentresult-message) | [`Error`](#error-message)
* [`CloseDocument`](#closedocument-message) => [`Ok`](#ok-message) | [`Error`](#error-message)
* [`ChangeText`](#changetext-message) => [`ChangeTextResult`](#changetextresult-message) | [`Error`](#error-message)
* [`MoveCursor`](#movecursor-message) => [`MoveCursorResult`](#movecursorresult-message) | [`Error`](#error-message)
* [`PatternSamples`](#patternsamples-message)
  * [`PatternSamples item object`](#patternsamples-item-object)
  * [`PatternSamples multi line item object`](#patternsamples-multi-line-item-object)
  * [`PatternSamples anchors object`](#patternsamples-anchors-object)
    * [`PatternSamples anchor items object`](#patternsamples-anchor-items-object)
  * [`PatternSamples separators object`](#patternsamples-separators-object)
    * [`PatternSamples separator items object`](#patternsamples-separator-items-object)
* [`GetPatternSamples`](#getpatternsamples-message) => [`PatternSamples`](#patternsamples-message) | [`Error`](#error-message)
* [`TextTransformations`](#texttransformations-message)
  * [`TextTransformations item object`](#texttransformations-item-object)
  * [`TextTransformations extended item object`](#texttransformations-extended-item-object)
  * [`TextTransformations diff item object`](#texttransformations-diff-item-object)
* [`GetTextTransformations`](#gettexttransformations-message) => [`TextTransformations`](#texttransformations-message) | [`Error`](#error-message)

---

### `Ok` message

|      Field     |      Type      | Mandatory Value |               Example              |
|:---------------|:---------------|:---------------:|:-----------------------------------|
| type           | String         |      "Ok"       |                                    |
| apiVersion     | String         |                 | "1.0.6"                            |

#### Example

```json
{
  "type": "Ok",
  "apiVersion": "1.0.6"
}
```

### `Error` message

|      Field     |      Type      | Mandatory Value |               Example              |
|:---------------|:---------------|:---------------:|:-----------------------------------|
| type           | String         |    "Error"      |                                    |
| apiVersion     | String         |                 | "1.0.6"                            |
| errorMessage   | String         |                 | "Error message"                    |

#### Example

```json
{
  "type": "Error",
  "apiVersion": "1.0.6",
  "errorMessage": "Error message"
}
```

### `Ping` message

|      Field     |      Type      | Mandatory Value |               Example              |
|:---------------|:---------------|:---------------:|:-----------------------------------|
| type           | String         |     "Ping"      |                                    |
| apiVersion     | String         |                 | "1.0.6"                            |
| sessionId      | String(32 hex) |                 | "bcd7219fdea8278cdfe728edf2781acd" |
| timestamp      | Long(ms)       |                 | 1488835645000                      |

#### Example

```json
{
  "type": "Ping",
  "apiVersion": "1.0.6",
  "sessionId": "bcd7219fdea8278cdfe728edf2781acd",
  "timestamp": 1488835645000
}
```

### `Pong` message

|      Field     |      Type     | Mandatory Value |    Example    |
|:---------------|:--------------|:---------------:|:--------------|
| type           | String        |     "Pong"      |               |
| apiVersion     | String        |                 | "1.0.6"       |
| timestamp      | Long          |                 | 1488835645000 |
| statuses       | Array[Object] |                 |               |

#### `Statuses` array

|     Field     |      Type     | Mandatory Value |    Example      |
|:--------------|:--------------|:---------------:|:----------------|
| document      | String        |                 | "documentPath1" |
| jobs          | Map[Object]   |                 |                 |

#### `jobs` map possible keys

* "patternsProcessing"
* "textTransformationsGenerating"

#### `jobs` object (value)

|   Field   |   Type   |         Mandatory Value         |    Example      |
|:----------|:---------|:-------------------------------:|:----------------|
| type      |  String  | "None", "Running" or "Finished" |                 |
| timestamp | Long(ms) |                                 | 1488835646000   |

#### Example

```json
{
  "type": "Pong",
  "apiVersion": "1.0.6",
  "timestamp": 1488835645000,
  "statuses" : [
    {
      "document" : "documentPath1",
      "jobs" : {
        "patternsProcessing" : { "type" : "Running", "timestamp" : 0 },
        "textTransformationsGenerating" : { "type" : "None", "timestamp" : 0 }
      }
    },
    {
      "document" : "documentPath2",
      "jobs" : {
        "patternsProcessing" : { "type" : "Finished", "timestamp" : 1488835646000 },
        "textTransformationsGenerating" : { "type" : "Running", "timestamp" : 0 }
      }
    }
  ]
}
```

### `GetSession` message

|      Field     |      Type      | Mandatory Value |               Example              |
|:---------------|:---------------|:---------------:|:-----------------------------------|
| type           | String         |   "GetSession"  |                                    |
| apiVersion     | String         |                 | "1.0.6"                            |
| application    | String         |                 | "SmartEditor"                      |
| applicationKey | String(32 hex) |                 | "1db76251256adfe71263bcdf2312bfac" |

#### Example

```json
{
  "type": "GetSession",
  "apiVersion": "1.0.6",
  "application": "SmartEditor",
  "applicationKey": "1db76251256adfe71263bcdf2312bfac"
}
```

### `Session` (`GetSession` message's result)

|      Field     |      Type      | Mandatory Value |               Example              |
|:---------------|:---------------|:---------------:|:-----------------------------------|
| type           | String         |    "Session"    |                                    |
| apiVersion     | String         |                 | "1.0.6"                            |
| sessionId      | String(32 hex) |                 | "bcd7219fdea8278cdfe728edf2781acd" |

#### Example

```json
{
  "type": "Session",
  "apiVersion": "1.0.6",
  "sessionId": "bcd7219fdea8278cdfe728edf2781acd"
}
```

### `OpenDocumentResult` message

|        Field        |      Type      |     Mandatory Value    |               Example              |
|:--------------------|:---------------|:----------------------:|:-----------------------------------|
| type                | String         |  "OpenDocumentResult"  |                                    |
| apiVersion          | String         |                        | "1.0.6"                            |
| statuses            | Object         |                        |                                    |

#### Example

```json
{
  "type": "OpenDocumentResult",
  "apiVersion": "1.0.6",
  "statuses" : {
    "document" : "documentPath",
    "jobs" : {
      "patternsProcessing" : { "type" : "Running", "timestamp" : 0 },
      "textTransformationsGenerating" : { "type" : "None", "timestamp" : 0 }
    }
  }
}
```

### `OpenDocument` message

|        Field         |        Type        |  Mandatory Value |               Example              |
|:---------------------|:-------------------|:----------------:|:-----------------------------------|
| type                 | String             |  "OpenDocument"  |                                    |
| apiVersion           | String             |                  | "1.0.6"                            |
| sessionId            | String(32 hex)     |                  | "bcd7219fdea8278cdfe728edf2781acd" |
| path                 | String             |                  | "C:/docs/doc.txt"                  |
| visibleText          | Array[String]      |                  | ["Some text", "123", "text2"]      |
| visibleTextStartLine | Unsigned Long Long |                  | 0                                  |
| cursorPos            | Object             |                  | {"lineIndex" : 0, "column" : 3}    |

#### Example

```json
{
  "type": "OpenDocument",
  "apiVersion": "1.0.6",
  "sessionId": "bcd7219fdea8278cdfe728edf2781acd",
  "path": "C:/docs/doc.txt",
  "visibleText": ["Some text", "123", "text2"],
  "visibleTextStartLine": 0,
  "cursorPos": {"lineIndex" : 0, "column" : 3}
}
```

### `CloseDocument` message

|      Field     |      Type      |  Mandatory Value  |               Example              |
|:---------------|:---------------|:-----------------:|:-----------------------------------|
| type           | String         |  "CloseDocument"  |                                    |
| apiVersion     | String         |                   | "1.0.6"                            |
| sessionId      | String(32 hex) |                   | "bcd7219fdea8278cdfe728edf2781acd" |
| path           | String         |                   | "C:/docs/doc.txt"                  |

#### Example

```json
{
  "type": "CloseDocument",
  "apiVersion": "1.0.6",
  "sessionId": "bcd7219fdea8278cdfe728edf2781acd",
  "path": "C:/docs/doc.txt"
}
```

### `ChangeTextResult` message

|        Field        |      Type      |    Mandatory Value   |               Example              |
|:--------------------|:---------------|:--------------------:|:-----------------------------------|
| type                | String         |  "ChangeTextResult"  |                                    |
| apiVersion          | String         |                      | "1.0.6"                            |
| statuses            | Object         |                      |                                    |

#### Example

```json
{
  "type": "ChangeTextResult",
  "apiVersion": "1.0.6",
  "statuses" : {
    "document" : "documentPath",
    "jobs" : {
      "patternsProcessing" : { "type" : "Running", "timestamp" : 0 },
      "textTransformationsGenerating" : { "type" : "None", "timestamp" : 0 }
    }
  }
}
```

### `ChangeText` message

|        Field         |        Type        |  Mandatory Value |               Example              |
|:---------------------|:-------------------|:----------------:|:-----------------------------------|
| type                 | String             |   "ChangeText"   |                                    |
| apiVersion           | String             |                  | "1.0.6"                            |
| sessionId            | String(32 hex)     |                  | "bcd7219fdea8278cdfe728edf2781acd" |
| path                 | String             |                  | "C:/docs/doc.txt"                  |
| visibleText          | Array[String]      |                  | ["Some text", "123", "text2"]      |
| visibleTextStartLine | Unsigned Long Long |                  | 0                                  |
| cursorPos            | Object             |                  | {"lineIndex" : 0, "column" : 3}    |

#### Example

```json
{
  "type": "ChangeText",
  "apiVersion": "1.0.6",
  "sessionId": "bcd7219fdea8278cdfe728edf2781acd",
  "path": "C:/docs/doc.txt",
  "visibleText": ["Some text", "123", "text2"],
  "visibleTextStartLine": 0,
  "cursorPos": {"lineIndex" : 0, "column" : 3}
}
```

### `MoveCursorResult` message

|        Field        |      Type      |    Mandatory Value   |               Example              |
|:--------------------|:---------------|:--------------------:|:-----------------------------------|
| type                | String         |  "MoveCursorResult"  |                                    |
| apiVersion          | String         |                      | "1.0.6"                            |
| statuses            | Object         |                      |                                    |

#### Example

```json
{
  "type": "MoveCursorResult",
  "apiVersion": "1.0.6",
  "statuses" : {
    "document" : "documentPath",
    "jobs" : {
      "patternsProcessing" : { "type" : "Running", "timestamp" : 0 },
      "textTransformationsGenerating" : { "type" : "None", "timestamp" : 0 }
    }
  }
}
```

### `MoveCursor` message

|        Field         |      Type      |  Mandatory Value |               Example              |
|:---------------------|:---------------|:----------------:|:-----------------------------------|
| type                 | String         |   "MoveCursor"   |                                    |
| apiVersion           | String         |                  | "1.0.6"                            |
| sessionId            | String(32 hex) |                  | "bcd7219fdea8278cdfe728edf2781acd" |
| document             | String         |                  | "C:/docs/doc.txt"                  |
| cursorPos            | Object         |                  | {"lineIndex" : 0, "column" : 3}    |

#### Example

```json
{
  "type": "MoveCursor",
  "apiVersion": "1.0.6",
  "sessionId": "bcd7219fdea8278cdfe728edf2781acd",
  "document": "C:/docs/doc.txt",
  "cursorPos": {"lineIndex" : 0, "column" : 3}
}
```

### `PatternSamples` message

|      Field     |        Type        |  Mandatory Value  | Example  |
|:---------------|:-------------------|:-----------------:|:---------|
| type           | String             | "PatternSamples"  |          |
| apiVersion     | String             |                   | "1.0.6"  |
| items          | Array[Object]      |                   |          |
| multiLineItems | Array[Object]      |                   |          |
| anchors        | Array[Object]      |                   |          |
| separators     | Array[Object]      |                   |          |

#### `PatternSamples` item object

|      Field     |        Type        |  Mandatory Value  | Example  |                           Description                           |
|:---------------|:-------------------|:-----------------:|:---------|:----------------------------------------------------------------|
| lineIndex      | Unsigned Long Long |                   | 0        | The pattern sample's line index. Starts from 0                  |

#### `PatternSamples` multi line item object

|      Field     |        Type        |  Mandatory Value  | Example  |                           Description                           |
|:---------------|:-------------------|:-----------------:|:---------|:----------------------------------------------------------------|
| fromLine       | Unsigned Long Long |                   | 0        | The multi line pattern sample's start line index. Starts from 0 |
| toLine         | Unsigned Long Long |                   | 1        | The multi line pattern sample's end line index. Starts from 0   |

#### `PatternSamples` anchors object

|      Field     |        Type        |  Mandatory Value  | Example  |                           Description                           |
|:---------------|:-------------------|:-----------------:|:---------|:----------------------------------------------------------------|
| lineIndex      | Unsigned Long Long |                   | 0        | The anchor groups record line index. Starts from 0              |
| items          | Array[Object]      |                   |          | The anchor groups array                                         |

##### `PatternSamples` anchor items object

|      Field     |        Type        |  Mandatory Value  | Example  |                           Description                           |
|:---------------|:-------------------|:-----------------:|:---------|:----------------------------------------------------------------|
| from           | Unsigned Long Long |                   | 0        | The current anchor group start pos in a line. Starts from 0     |
| to             | Unsigned Long Long |                   | 1        | The current anchor group end pos in a line. Starts from 0       |

#### `PatternSamples` separators object

|      Field     |        Type        |  Mandatory Value  | Example  |                           Description                           |
|:---------------|:-------------------|:-----------------:|:---------|:----------------------------------------------------------------|
| lineIndex      | Unsigned Long Long |                   | 0        | The separator groups record line index. Starts from 0           |
| items          | Array[Object]      |                   |          | The separator groups array                                      |

##### `PatternSamples` separator items object

|      Field     |        Type        |  Mandatory Value  | Example  |                           Description                           |
|:---------------|:-------------------|:-----------------:|:---------|:----------------------------------------------------------------|
| from           | Unsigned Long Long |                   | 0        | The current separator group start pos in a line. Starts from 0  |
| to             | Unsigned Long Long |                   | 1        | The current separator group end pos in a line. Starts from 0    |

#### Example

```json
{
  "type": "PatternSamples",
  "apiVersion": "1.0.6",
  "items": [
    {"lineIndex": 0}
  ],
  "multiLineItems": [
    {"fromLine": 0, "toLine": 0}
  ],
  "anchors": [
    {"lineIndex": 0, "items": [{"from": 0, "to": 3}, {"from": 5, "to": 7}, {"from": 20, "to": 22}]},
    {"lineIndex": 1, "items": [{"from": 0, "to": 1}, {"from": 3, "to": 8}, {"from": 10, "to": 12}]},
    {"lineIndex": 2, "items": [{"from": 0, "to": 7}, {"from": 9, "to": 15}, {"from": 24, "to": 29}]}
  ],
  "separators": [
    {"lineIndex": 0, "items": [{"from": 4, "to": 4}, {"from": 8, "to": 19}]},
    {"lineIndex": 1, "items": [{"from": 2, "to": 2}, {"from": 9, "to": 9}]},
    {"lineIndex": 2, "items": [{"from": 8, "to": 8}, {"from": 16, "to": 23}]}
  ]
}
```

### `GetPatternSamples` message

|      Field     |      Type      |   Mandatory Value   |               Example              |
|:---------------|:---------------|:-------------------:|:-----------------------------------|
| type           | String         | "GetPatternSamples" |                                    |
| apiVersion     | String         |                     | "1.0.6"                            |
| sessionId      | String(32 hex) |                     | "bcd7219fdea8278cdfe728edf2781acd" |
| document       | String         |                     | "Document1Path"                    |

#### Example

```json
{
  "type": "GetPatternSamples",
  "apiVersion": "1.0.6",
  "sessionId": "bcd7219fdea8278cdfe728edf2781acd",
  "document": "Document1Path"
}
```

### `TextTransformations` message

|      Field     |        Type        |    Mandatory Value    | Example  |
|:---------------|:-------------------|:---------------------:|:---------|
| type           | String             | "TextTransformations" |          |
| apiVersion     | String             |                       | "1.0.6"  |
| items          | Array[Object]      |                       |          |
| extended       | Array[Object]      |                       |          |

#### `TextTransformations` item object

|      Field     |        Type        |  Mandatory Value  | Example       |          Description                                |
|:---------------|:-------------------|:-----------------:|:--------------|:----------------------------------------------------|
| lineIndex      | Unsigned Long Long |                   | 3             | The text transformation's line index. Starts from 0 |
| text           | String             |                   | "Some text 3" | The text that replaces the old text.                |

#### `TextTransformations` extended item object

|      Field     |        Type        |  Mandatory Value  | Example       |          Description                                |
|:---------------|:-------------------|:-----------------:|:--------------|:----------------------------------------------------|
| lineIndex      | Unsigned Long Long |                   | 3             | The text transformation's line index. Starts from 0 |
| diff           | Array[Object]      |                   |               |                                                     |

#### `TextTransformations` diff item object

|  Field |  Type  |         Mandatory Value         |    Example   |  Description  |
|:-------|:-------|:-------------------------------:|:-------------|:--------------|
| type   | String | "Add", "Remove" or "DontChange" | "DontChange" | The diff type |
| text   | String |                                 | "Some text"  |               |

#### Example

```json
{
  "type": "TextTransformations",
  "apiVersion": "1.0.6",
  "items": [
    {"lineIndex": 7, "text": "Some text 1"},
    {"lineIndex": 3, "text": "Some text 2"},
    {"lineIndex": 9, "text": "Some text 3"}
  ],
  "extended": [
    {"lineIndex": 7, "diff" : [{"type" : "DontChange", "text" : "Some text "}, {"type" : "Remove", "text" : "a"}, {"type" : "Add", "text" : "1"}]},
    {"lineIndex": 3, "diff" : [{"type" : "DontChange", "text" : "Some text "}, {"type" : "Remove", "text" : "bb"}, {"type" : "Add", "text" : "2"}]},
    {"lineIndex": 9, "diff" : [{"type" : "DontChange", "text" : "Some text "}, {"type" : "Remove", "text" : "ccc"}, {"type" : "Add", "text" : "3"}]}
  ]
}
```

### `GetTextTransformations` message
 
|      Field     |      Type      |      Mandatory Value     |               Example              |
|:---------------|:---------------|:------------------------:|:-----------------------------------|
| type           | String         | "GetTextTransformations" |                                    |
| apiVersion     | String         |                          | "1.0.6"                            |
| sessionId      | String(32 hex) |                          | "bcd7219fdea8278cdfe728edf2781acd" |
| document       | String         |                          | "Document1Path"                    |

