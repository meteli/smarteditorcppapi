## SmartEditor C++ API
`v1.0.7`

### Dependencies
* internal:
  * PicoSHA2
  * fmt `v5.3.0`
* external:
  * Boost `v1.66+`
  
### Docs
* [Protocol](docs/protocol.md)

<!-- 2022.12.11 Just in case GitLab doesn't delete the project due to inactivity. -->
